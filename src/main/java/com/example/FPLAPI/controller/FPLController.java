package com.example.FPLAPI.controller;

import com.example.FPLAPI.service.FPLService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FPLController {
    @Autowired
    private FPLService fplService;

    @GetMapping("/hello")
    public String helloWorld() { return "hello world!"; }

    @GetMapping("/league/{leagueID}")
    public String getTopPlayerName(@PathVariable int leagueID) throws JsonProcessingException {
        String playerData = fplService.getTopPlayerName(leagueID);
        return playerData;
    }
}
