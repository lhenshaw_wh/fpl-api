package com.example.FPLAPI.controller;

import com.example.FPLAPI.model.PlayerStatsDTO;
import com.example.FPLAPI.service.PlayerStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController

@RequestMapping("/api/player-stats")
public class PlayerStatsController {
    @Autowired
    private PlayerStatsService playerStatsService;


    @GetMapping("/{playerId}")
    @Cacheable(value = "playerStatsCache", key = "#playerId")
    public PlayerStatsDTO getPlayerStatsData(@PathVariable int playerId) throws IOException {
        return playerStatsService.getPlayerStats(playerId);
    }

    @GetMapping("/{playerId}/{playerId2}")
    public List<PlayerStatsDTO> get2PlayersStatsData(@PathVariable("playerId") int firstPlayerId, @PathVariable("playerId2") int secondPlayerId) throws IOException {
        return playerStatsService.get2PlayersStats(firstPlayerId, secondPlayerId);
    }
}
