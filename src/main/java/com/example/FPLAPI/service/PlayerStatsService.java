package com.example.FPLAPI.service;

import com.example.FPLAPI.model.PlayerStatsDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlayerStatsService {
    private RestTemplate restTemplate = new RestTemplate();
    private ObjectMapper objectMapper = new ObjectMapper();
    private String PLAYER_STATS_URL = "https://fantasy.premierleague.com/api/bootstrap-static/";
    private ResponseEntity<String> response = restTemplate.getForEntity(PLAYER_STATS_URL, String.class);
    private String responseBody = response.getBody();
    private JsonNode playersParentNode = objectMapper.readTree(responseBody).path("elements");
    public PlayerStatsService() throws JsonProcessingException {
    }

    @Cacheable(value = "playerStatsCache", key = "#playerId")
    public PlayerStatsDTO getPlayerStats(int playerId) throws JsonProcessingException {


        for (JsonNode player : playersParentNode) {
            int id = player.get("id").asInt();
            if (id == playerId) {
                PlayerStatsDTO playerStatsDTO = objectMapper.treeToValue(player, PlayerStatsDTO.class);
                return playerStatsDTO;
            }
        }
        return null;
    }
    @Cacheable(value = "playerStatsCache", key = "#firstPlayerId + '-' + #secondPlayerId")
    public List<PlayerStatsDTO> get2PlayersStats(int firstPlayerId, int secondPlayerId) throws JsonProcessingException {
        PlayerStatsDTO firstPlayerStats = null;
        PlayerStatsDTO secondPlayerStats = null;

        for (JsonNode player : playersParentNode) {
            int id = player.get("id").asInt();
            if (id == firstPlayerId) {
                firstPlayerStats = objectMapper.treeToValue(player, PlayerStatsDTO.class);
            } else if (id == secondPlayerId) {
                secondPlayerStats = objectMapper.treeToValue(player, PlayerStatsDTO.class);
            }
            if (firstPlayerStats != null && secondPlayerStats != null) {
                break;
            }
        }

        if (firstPlayerStats == null || secondPlayerStats == null) {
            return null;
        }

        List<PlayerStatsDTO> playerStatsList = new ArrayList<>();
        playerStatsList.add(firstPlayerStats);
        playerStatsList.add(secondPlayerStats);

        return playerStatsList;
    }
}

