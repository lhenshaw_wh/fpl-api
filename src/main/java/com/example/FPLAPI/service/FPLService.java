package com.example.FPLAPI.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FPLService {

    public String getTopPlayerName(int leagueID) throws JsonProcessingException {
        String PLAYER_DATA_API_URL = "https://fantasy.premierleague.com/api/leagues-classic/" + leagueID + "/standings/";
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.getForEntity(PLAYER_DATA_API_URL, String.class);
        String responseBody = response.getBody();

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(responseBody);
        JsonNode standings = root.path("standings");
        JsonNode topPlayer = standings.path("results").get(0);
        String realName = topPlayer.path("player_name").asText();
        String topPlayerName = topPlayer.path("entry_name").asText();

        String topPlayerResponse = "The top player is: " + realName + ". Team Name: "+ topPlayerName;

        return topPlayerResponse;
    }
}
//114493