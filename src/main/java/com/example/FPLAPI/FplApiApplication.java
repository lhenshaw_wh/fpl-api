package com.example.FPLAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.example")
@EnableCaching
public class FplApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FplApiApplication.class, args);
	}

}
