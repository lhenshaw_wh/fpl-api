package com.example.FPLAPI.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayerStatsDTO {
    @JsonProperty("id")
    private int id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("second_name")
    private String secondName;

    @JsonProperty("now_cost")
    private double nowCost;

    @JsonProperty("total_points")
    private int totalPoints;

    @JsonProperty("points_per_game")
    private double pointsPerGame;

    @JsonProperty("minutes")
    private int minutesPlayed;

    @JsonProperty("goals_scored")
    private int goalsScored;

    @JsonProperty("assists")
    private int assists;

    @JsonProperty("goals_conceded")
    private int goalsConceded;

    @JsonProperty("expected_goals_per_90")
    private double expectedGoalsPer90;

    @JsonProperty("expected_assists_per_90")
    private double expectedAssistsPer90;

    @JsonProperty("yellow_cards")
    private int yellowCards;

    @JsonProperty("red_cards")
    private int redCards;

    public double getNowCost() {
        return nowCost / 10;
    }
    public PlayerStatsDTO() {
        // Default constructor for Jackson deserialization
    }

    public PlayerStatsDTO(int id, String firstName, String secondName, double nowCost, int totalPoints, double pointsPerGame, int minutesPlayed,
                          int goalsScored, int assists, int goalsConceded, double expectedGoalsPer90, double expectedAssistsPer90,
                          int yellowCards, int redCards) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.nowCost = nowCost;
        this.totalPoints = totalPoints;
        this.pointsPerGame = pointsPerGame;
        this.minutesPlayed = minutesPlayed;
        this.goalsScored = goalsScored;
        this.assists = assists;
        this.goalsConceded = goalsConceded;
        this.expectedGoalsPer90 = expectedGoalsPer90;
        this.expectedAssistsPer90 = expectedAssistsPer90;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
    }
}

