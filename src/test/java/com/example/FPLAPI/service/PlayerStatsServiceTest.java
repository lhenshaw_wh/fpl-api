package com.example.FPLAPI.service;

import com.example.FPLAPI.model.PlayerStatsDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.mockito.Mockito.when;

class PlayerStatsServiceTest {
    private PlayerStatsService playerStatsService;
    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    public void setup() throws JsonProcessingException {
        MockitoAnnotations.openMocks(this);
        playerStatsService = new PlayerStatsService();
    }

    @Test
    void getPlayerStats() throws JsonProcessingException {
        //Arrange, Act, Assert
        String apiResponse = "{ \"elements\": [{ \"id\": 335, \"first_name\": \"Marcus\", \"second_name\": \"Rashford\", \"now_cost\": 72, \"minutes\": 2743, \"goals_scored\": 16, \"assists\": 7, \"goals_conceded\": 37, \"expected_goals\": \"13.38\", \"expected_assists\": \"2.68\", \"yellow_cards\": 2, \"red_cards\": 0 }] }";
        ResponseEntity<String> responseEntity = ResponseEntity.ok(apiResponse);
        when(restTemplate.getForEntity("https://fantasy.premierleague.com/api/bootstrap-static/", String.class))
                .thenReturn(responseEntity);

        PlayerStatsDTO playerStatsDTO = playerStatsService.getPlayerStats(335);

        Assertions.assertNotNull(playerStatsDTO);
        Assertions.assertEquals("Marcus", playerStatsDTO.getFirstName());
        Assertions.assertEquals("Rashford",playerStatsDTO.getSecondName());
        Assertions.assertEquals(7.2, playerStatsDTO.getNowCost());
        Assertions.assertEquals(2743, playerStatsDTO.getMinutesPlayed());
        Assertions.assertEquals(16, playerStatsDTO.getGoalsScored());
        Assertions.assertEquals(7, playerStatsDTO.getAssists());
        Assertions.assertEquals(37, playerStatsDTO.getGoalsConceded());
        Assertions.assertEquals(0.44, playerStatsDTO.getExpectedGoalsPer90());
        Assertions.assertEquals(0.09, playerStatsDTO.getExpectedAssistsPer90());
        Assertions.assertEquals(2, playerStatsDTO.getYellowCards());
        Assertions.assertEquals(0, playerStatsDTO.getRedCards());
    }
    @Test
    public void testGet2PlayersStats() throws JsonProcessingException {
        String apiResponse = "{ \"elements\": [{ \"id\": 335, \"first_name\": \"Marcus\", \"second_name\": \"Rashford\", \"now_cost\": 72, \"minutes\": 2743, \"goals_scored\": 16, \"assists\": 7, \"goals_conceded\": 37, \"expected_goals\": \"13.38\", \"expected_assists\": \"2.68\", \"yellow_cards\": 2, \"red_cards\": 0 }, { \"id\": 318, \"first_name\": \"Erling\", \"second_name\": \"Haaland\", \"now_cost\": 124, \"minutes\": 2661, \"goals_scored\": 36, \"assists\": 8, \"goals_conceded\": 25, \"expected_goals\": \"27.33\", \"expected_assists\": \"2.89\", \"yellow_cards\": 5, \"red_cards\": 0 }] }";
        ResponseEntity<String> responseEntity = ResponseEntity.ok(apiResponse);
        when(restTemplate.getForEntity("https://fantasy.premierleague.com/api/bootstrap-static/", String.class))
                .thenReturn(responseEntity);

        List<PlayerStatsDTO> playerStatsList = playerStatsService.get2PlayersStats(335, 318);

        Assertions.assertNotNull(playerStatsList);
        Assertions.assertEquals(2, playerStatsList.size());

        PlayerStatsDTO firstPlayerStats = playerStatsList.get(0);
        Assertions.assertEquals("Marcus", firstPlayerStats.getFirstName());
        Assertions.assertEquals("Rashford", firstPlayerStats.getSecondName());
        Assertions.assertEquals(7.2, firstPlayerStats.getNowCost());
        Assertions.assertEquals(2743, firstPlayerStats.getMinutesPlayed());
        Assertions.assertEquals(16, firstPlayerStats.getGoalsScored());
        Assertions.assertEquals(7, firstPlayerStats.getAssists());
        Assertions.assertEquals(37, firstPlayerStats.getGoalsConceded());
        Assertions.assertEquals(0.44, firstPlayerStats.getExpectedGoalsPer90());
        Assertions.assertEquals(0.09, firstPlayerStats.getExpectedAssistsPer90());
        Assertions.assertEquals(2, firstPlayerStats.getYellowCards());
        Assertions.assertEquals(0, firstPlayerStats.getRedCards());

        PlayerStatsDTO secondPlayerStats = playerStatsList.get(1);
        Assertions.assertEquals("Erling", secondPlayerStats.getFirstName());
        Assertions.assertEquals("Haaland", secondPlayerStats.getSecondName());
        Assertions.assertEquals(12.4, secondPlayerStats.getNowCost());
        Assertions.assertEquals(2661, secondPlayerStats.getMinutesPlayed());
        Assertions.assertEquals(36, secondPlayerStats.getGoalsScored());
        Assertions.assertEquals(8, secondPlayerStats.getAssists());
        Assertions.assertEquals(25, secondPlayerStats.getGoalsConceded());
        Assertions.assertEquals(0.92, secondPlayerStats.getExpectedGoalsPer90());
        Assertions.assertEquals(0.10, secondPlayerStats.getExpectedAssistsPer90());
        Assertions.assertEquals(5, secondPlayerStats.getYellowCards());
        Assertions.assertEquals(0, secondPlayerStats.getRedCards());
    }
}